"""
Ben Hegman
05/29/2020
Python 3.7
"""

import os
import pandas as pd
import geopandas as gpd
from shapely.geometry import MultiPoint
from shapely.ops import nearest_points
import numpy as np
# set your working directory here to the location containing the two csv files to be used
# os.chdir(<yourWorkingDirectory>)

# a function that takes all query points in a csv and returns the closest building(s) from a csv of buildings
# in this example, query points is our queries.csv and buildings is buildings.csv
def nearest_buildings(query_points, buildings):
    # read each csv as a pandas dataframe
    queries = pd.read_csv(query_points)
    buildings = pd.read_csv(buildings)

    # convert coords to a Point object and add to the dataframe - making it a geodataframe
    gdfOrig = gpd.GeoDataFrame(
        queries, geometry=gpd.points_from_xy(queries.Y, queries.X)
    )
    gdfDest = gpd.GeoDataFrame(
        buildings, geometry=gpd.points_from_xy(buildings.Y, buildings.X)
    )

    # make a list of the points to be queried and of the building points
    queryList = list(gdfOrig["geometry"])
    destList = list(gdfDest["geometry"])
    # change building point list to a multipoint for nearest_point function
    destMultiPoint = MultiPoint(destList)

    # finds the closest building and the height for each query point and saves each to a list
    closestBuildings = [
        np.array2string(
            gdfDest.loc[
                gdfDest["geometry"] == nearest_points(point, destMultiPoint)[1]
            ].Name.values,
            separator=", ",
        )
        for point in queryList
    ]
    buildingHeight = [
        np.array2string(
            gdfDest.loc[
                gdfDest["geometry"] == nearest_points(point, destMultiPoint)[1]
            ].Height.values,
            separator=", ",
        )
        for point in queryList
    ]

    # add new columns before saving to csv
    # we could include any other building info here by doing gdfOrig[<columnName>] = columnList
    gdfOrig["Closest Major Building"] = closestBuildings
    gdfOrig["Building Height"] = buildingHeight
    # save to a csv in the same location called 'queried.csv'
    gdfOrig.to_csv("queried.csv")


# call the function with our two csv files
nearest_buildings("queries.csv", "buildings.csv")

# a function that returns all buildings within a user specified distance of the query point in ascending order
def building_distance_ranks(query_points, buildings, distance):
    # read each csv as a pandas dataframe
    queries = pd.read_csv(query_points)
    buildings = pd.read_csv(buildings)

    # convert coords to a Point object and add to the dataframe - making it a geodataframe
    gdfOrig = gpd.GeoDataFrame(
        queries, geometry=gpd.points_from_xy(queries.Y, queries.X)
    )
    gdfDest = gpd.GeoDataFrame(
        buildings, geometry=gpd.points_from_xy(buildings.Y, buildings.X)
    )

    # put the query/building names and points in a dictionary to make it easier to reference
    queryList = dict(zip(gdfOrig.Name, gdfOrig.geometry))
    destList = dict(zip(gdfDest.Name, gdfDest.geometry))

    # nested loop that takes each queried point and checks the distance of each building to that point
    # if the distance is less than the specified value, the building is included in the output
    distanceDict = {}
    for location, point in queryList.items():
        buildingDict = {}
        for name, building in destList.items():
            if point.distance(building) < distance:
                buildingDict[name] = point.distance(building)
        # sorts the distances in ascending order
        sortedDict = {
            k: v for k, v in sorted(buildingDict.items(), key=lambda item: item[1])
        }
        distanceDict[location] = sortedDict

    # add new column before saving to csv
    gdfOrig["Buildings within " + str(distance) + " feet"] = distanceDict.values()
    gdfOrig.to_csv("queriedByDistance.csv")


# call the function with our two csv files and a specified distance
building_distance_ranks("queries.csv", "buildings.csv", 4500)
