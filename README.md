# CSI Coding Exercise

The following python script contains two functions. One takes two csv files which contain a list of query points and a list of the tallest buildings in Portland as input, and finds the closest building(s) to each query point. The second takes the same two csv files and a user specified distance and returns a list of buildings within that distance to each query point.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the requirements. To do this, activate a new or desired Python3 virtual environment in your terminal and run the following command.

```bash
pip install -r requirements.txt
```

## Running

To run the script, first make sure you're in the correct working directory - the folder of the script and csv files, then run the following code. The outputs will be saved as two csv files titled 'queried.csv' and 'queriedByDistance.csv' to the same folder. To change the distance at which the second function will return building names up to, open closestBuildings.py and change the specified distance in line 106.

```bash
python closestBuildings.py
```
